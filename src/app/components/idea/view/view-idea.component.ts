import { Component, OnInit } from '@angular/core';
import { Idea } from '../../../model/idea';
import { IdeaService } from '../../../services/idea.service';
import { ActivatedRoute } from '@angular/router';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-view-idea',
  templateUrl: './view-idea.component.html',
  styleUrls: ['./view-idea.component.scss'],
})
export class ViewIdeaComponent implements OnInit {
  spinnerIcon = faSpinner;

  loading: boolean;
  loadingError: any;
  idea: Idea;

  constructor(
    private ideaService: IdeaService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.loadIdeaData();
  }

  caseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }

  loadIdeaData() {
    this.loading = true;
    this.route.params.subscribe((params) => {
      this.ideaService
        .get(params.ideaId)
        .pipe(finalize(() => (this.loading = false)))
        .subscribe(
          (idea) => (this.idea = idea),
          (error) => (this.loadingError = error)
        );
    });
  }
}
