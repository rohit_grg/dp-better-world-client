import { Component, OnInit } from '@angular/core';
import { IdeaService } from '../../../services/idea.service';
import { Idea } from '../../../model/idea';
import { debounceTime, finalize } from 'rxjs/operators';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { GOALS } from '../../../model/goal';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'app-add-idea',
  templateUrl: './add-idea.component.html',
  styleUrls: ['./add-idea.component.scss'],
})
export class AddIdeaComponent implements OnInit {
  spinnerIcon = faSpinner;
  goals = GOALS;

  addIdeaForm: FormGroup;
  loading: boolean;
  error: any;
  saving: boolean;

  idea: Idea;

  urlRegex: RegExp;

  constructor(
    private ideaService: IdeaService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.error = null;
    this.urlRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
  }

  ngOnInit(): void {
    this.addIdeaForm = this.formBuilder.group({
      title: ['', Validators.required],
      name: ['', Validators.required],
      sustainable: ['', Validators.required],
      ideaDescription: [
        '',
        Validators.compose([Validators.required, Validators.maxLength(1000)]),
      ],
      image: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(this.urlRegex),
        ]),
      ],
    });
  }

  onSubmit() {
    this.saving = true;

    this.idea = {
      description: this.ideaDescription.value,
      goal: this.sustainable.value,
      thumbnailUrl: this.image.value,
      title: this.title.value,
    };

    this.ideaService
      .post(this.idea)
      .pipe(finalize(() => (this.saving = false)))
      .subscribe(
        (data) => {
          this.router.navigate(['/thank-you']);
        },
        (error) => (this.error = error)
      );
  }

  get name() {
    return this.addIdeaForm.get('name');
  }

  get title() {
    return this.addIdeaForm.get('title');
  }

  get ideaDescription() {
    return this.addIdeaForm.get('ideaDescription');
  }

  get sustainable() {
    return this.addIdeaForm.get('sustainable');
  }

  get image() {
    return this.addIdeaForm.get('image');
  }

  get serverError() {
    return this.error;
  }
}
