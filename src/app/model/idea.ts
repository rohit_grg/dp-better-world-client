export interface Goal {
  key: string;
  label: string;
}

export interface Idea {
  id?: number;
  title: string;
  description: string;
  thumbnailUrl: string;
  goal: Goal;
  likes?: number;
}
